FROM openjdk:8-jdk-alpine
LABEL MAINTAINER="lenovodevops" 
RUN mkdir /home/longStorage
RUN touch /home/longStorage/environments.config
RUN mkdir /home/logStorage
RUN mkdir /home/yamlStorage
RUN mkdir /home/listStorage
COPY list.txt /home/listStorage
COPY services.yml /home/longStorage
RUN mkdir /root/.ssh/
COPY id_rsa /root/.ssh/
COPY id_rsa.pub /root/.ssh/
RUN chmod 755 /root/.ssh/
RUN chmod 600 /root/.ssh/id_rsa
ARG JAR_FILE
COPY ${JAR_FILE} deployment.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/deployment.jar"]
